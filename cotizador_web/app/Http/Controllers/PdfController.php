<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function prueba()
    {
        $html_content = '<h1>Generate a PDF using TCPDF in laravel </h1>
        		<h4>by<br/>Learn Infinity</h4>';
      
 
        PDF::SetTitle('Sample PDF');
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');
 
        PDF::Output('SamplePDF.pdf');
    }
     
    public function descargar(){    
        $html_content = '<h1>Generate a PDF using TCPDF in laravel </h1>
        		<h4>by<br/>Learn Infinity</h4>';
      
 
        PDF::SetTitle('Sample PDF');
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');
 
        PDF::Output(uniqid().'_SamplePDF.pdf', 'D');
    }

    public function preview(){
    	// $view = \View::make('previewPDF');
    	// return view('previewPDF');
    	$view = \View::make('previewPDF');
        $html_content = $view->render();
      
 
       PDF::SetTitle('Sample PDF');
       
        PDF::Image('img/empresa.png', 15, 140, 75, 113, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
       PDF::AddPage();
       PDF::writeHTML($html_content, true, false, true, false, '');
 
       PDF::Output(uniqid().'_SamplePDF.pdf');

	}
    
    public function guardar(){    
      
 
        PDF::SetTitle('Sample PDF');
        PDF::AddPage();
        PDF::writeHTML($this->html(), true, false, true, false, '');
 
        PDF::Output(public_path(uniqid().'_SamplePDF.pdf'), 'I');
    }
    public function html(){
    	$bandera = 'soy bandera';
        $html_content = '<h1>Generate a PDF using TCPDF in laravel </h1>
        		<h4>by<br/>Learn Infinity</h4> <br> <div class="text-center" style="text-align: center;"><p>lorem lorem lorem</p> <hr> <p>'.$bandera.'</p></div>';
        		return preview();
    }
}
