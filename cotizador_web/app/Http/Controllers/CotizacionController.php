<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cotizaciones;
use Illuminate\Support\Facades\DB;

class CotizacionController extends Controller{
    // $cotizaciones = Cotizacion::all();
    // return $cotizaciones;
    public function index(){
    	$cotizaciones = Cotizaciones::all();

    	return view('cotizaciones',[
    		'cotizaciones' => $cotizaciones,
    		'menu' => 'Cotizaciones'
    	]);
    }

    public function nuevo(){
        // $cotizacion = array();
        // $data =  array('nombre' =>'' ,'razon_soc'=>'','rfc'=>'','fecha_ven'=>'','plantilla_id'=>0,'id'=>0 );
        // $cotizacion=$data;
    	return view('new_cotizacion',[
            'menu' => 'Nueva cotización'
        ]);
        // ,compact('cotizacion')
        // return $cotizacion;
    }
    
    public function insertar(Request $request){
        if($request->isMethod('post')){
            $cotizacion =  new Cotizaciones;
            $cotizacion->nombre =$request->nombre_c;
            $cotizacion->razon_soc = $request->razon_soc;
            $cotizacion->rfc = $request->rfc;
            $cotizacion->fecha_ven = $request->fecha_ven;
            $cotizacion->concepto_id = 0;
            $cotizacion->terminos = 0;
            $cotizacion->plantilla_id = $request->txt_id_plantilla;
            $cotizacion->status = 'borrador';
            $id_cotizacion = $request->id_cotizacion;


            $data =['response' => 'fail','mensaje' => ''];
            $isRegistrer = DB::table('cotizaciones')->where('id',$id_cotizacion)->exists();

            if ($isRegistrer) {
                $isActualizado = Cotizaciones::where('id', $id_cotizacion)
                ->update([
                    'nombre' => $cotizacion->nombre,
                    'razon_soc' => $cotizacion->razon_soc,
                    'rfc' => $cotizacion->rfc,
                    'fecha_ven' => $cotizacion->fecha_ven,
                    'plantilla_id' => $cotizacion->plantilla_id
                ]);
                $data =[
                    'response' => 'success',
                    'mensaje' => 'actualizado',
                    'id_plantilla' => $cotizacion->plantilla_id,
                    'id' => $id_cotizacion
                ];
                return $data;
                exit;
            }else{
                $guardar = $cotizacion->save();
                $id_ultimo = Cotizaciones::latest('id')->first();
                if (json_decode($guardar)) {
                    $data =[
                        'response' => 'success',
                        'mensaje' => 'insertado',
                        'id' => $id_ultimo->id,
                        'id_plantilla' => $cotizacion->plantilla_id
                    ];
                    return $data;
                    exit;
                }
            }
            
        }else{
            $data =['response' => 'fail','mensaje' => 'no es post'];
            return $data;
        }
    }

    public function editar(Request $request){
        $data =['response' => 'fail','mensaje' => ''];
        $cotizacion = DB::table('cotizaciones')->where('id', $request->id_cotizacion)->get();
        
        if (count($cotizacion)) {
           $data =$cotizacion;
        }else{
            $data =['response' => 'fail','mensaje' => 'Concepto',"cotizacion" => $cotizacion];
        }

        return view('new_cotizacion',[
            'menu' => 'Nueva cotización',
            'cotizacion' => $data       
        ]);

        // return $data[0]->id;



    }
}
