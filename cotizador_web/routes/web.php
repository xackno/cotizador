<?php


// Auth::routes(['verify'=>true]);

Route::get('/', function () {
    // return view('auth/login');
    $menu="cotizador web";
    return view('forms/inicio',compact('menu'));
});

Auth::routes();



Route::middleware(['auth','verified'])->group(function () {
Route::get('/home','CotizacionController@index');
Route::get('/nuevo','CotizacionController@nuevo');
Route::post('/insert','CotizacionController@insertar');
Route::get('/editar/{id_cotizacion}','CotizacionController@editar');

Route::get('/indexConcepto','ConceptoController@index');
Route::post('/insertConcepto','ConceptoController@insertar');
Route::get('/editarConcepto','ConceptoController@editar');

Route::get('/indexTermino', 'TerminoController@index');
Route::post('/insertTermino','TerminoController@insertar');
Route::get('/editarTermino','TerminoController@editar');

Route::get('/PDF','PdfController@prueba');
Route::get('/PDF_guardar','PdfController@guardar');
Route::get('/PDF_descargar','PdfController@descargar');
Route::get('/PDF_preview','PdfController@preview');
});