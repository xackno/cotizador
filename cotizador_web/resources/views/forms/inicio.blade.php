@extends('layouts.app')

@section('content')


<div class="container" style="width: 100%">
	@if(isset($msj))
		<div class="alert alert-success alert-dismissible fade show">
			<p><b>{{$msj}}</b></p>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	<br><br>
	<h4>ENVÍA COTIZACIONES PROFESIONALES Y CONSIGUE MÁS PROYECTOS</h4> 
		 <p>
		 	Este es la herramienta ágil de cotización y facturación para emprendedores y pequeños negocios.
		 </p>



	<img src="{{asset('public/img/img-1.png')}}">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header bg-primary text-white">
					<b>COTIZACIONES</b>
				</div>
				<div class="card-body">
					<p> 
						¿Qué sistema utilizas para elaborar cotizaciones? ¿Word? ¿Excel? ¿Whatsapp? ¿Por qué utilizar dos sistemas para un mismo flujo de trabajo? Con este sistema  crearás cotizaciones y facturas en un mismo lugar.
					</p>
				</div>
			</div>
		</div><!-- fin card -->
		<div class="col">
			<div class="card">
				<div class="card-header bg-danger text-white">
					<b>PERSONALIZACIÓN</b>	
				</div>
				<div class="card-body">
				<p>Cambia la apariencia de tus cotizaciones y facturas de acuerdo a la identidad de tu marca 🎨 </p>		
				</div>
			</div>
		</div><!-- fin col -->

		
	</div><!-- fin row -->
<br>

	<div  style="background-color: #FDF2E9; text-align: center">
		<h3><b>PRECIOS</b></h3>
			<p>Crea tu cuenta en 30 segundos, prueba _____ gratis por 1 mes y elige tu plan en cualquier momento</p>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h3 class="text-primary">BÁSICO</h3>
						<h1><b>$40MXN</b></h1>
						<p>5 documentos al mes <br>Cotizaciones o facturas</p>
						
						<p>Personalización de colores y logotipo</p><hr>
						<p>Soporte de chat</p><hr>
						<p>Documento extra: $10.00 MXN</p>
					</div>
				</div>
			</div><!-- fin col -->
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h3 class="text-danger">PLUS</h3>
						<h1><b>$40MXN</b></h1>
						<p>5 documentos al mes <br>Cotizaciones o facturas</p>
						
						<p>Personalización de colores y logotipo</p><hr>
						<p>Soporte de chat</p><hr>
						<p>Documento extra: $10.00 MXN</p>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h3 class="text-info">PREMIUM</h3>
						<h1><b>$40MXN</b></h1>
						<p>5 documentos al mes <br>Cotizaciones o facturas</p>
						
						<p>Personalización de colores y logotipo</p><hr>
						<p>Soporte de chat</p><hr>
						<p>Documento extra: $10.00 MXN</p>
					</div>
				</div>
			</div>
			<br><br>
		</div><!-- fin row -->
	</div>
	







	<br><br><br><br>

</div><!-- fin container -->
@endsection
