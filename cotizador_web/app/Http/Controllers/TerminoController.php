<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Termino;

use Illuminate\Http\Request;

class TerminoController extends Controller
{
    public function index(Request $request){
    	$cotizacion_id = $request->id_cotizacion;
    	$conceptos = DB::table('terminos')->where('cotizacion_id', $cotizacion_id)->get();
      	return $conceptos;
    }
    
    public function insertar(Request $request){
        if($request->isMethod('post')){
            $termino =  new Termino;
            $termino->termino = $request->termino;
            $termino->cotizacion_id = $request->id_cotizacion;
            $cotizacion_id = $request->cotizacion_id;
            $id_termino = $request->id;


            $data =['response' => 'fail','mensaje' => ''];
            $isRegistrer = DB::table('terminos')->where('id',$id_termino)->exists();

            if ($isRegistrer) {
                $isActualizado = Termino::where('id', $id_termino)
                ->update([
                    'termino' => $termino->termino
                ]);
                $data =[
                    'response' => 'success',
                    'mensaje' => 'actualizado',
                    'id_concepto' => $id_termino
                ];
                return $data;
                exit;
            }else{
                $guardar = $termino->save();
                $id_ultimo = Termino::latest('id')->first();
                if (json_decode($guardar)) {
                    $data =[
                        'response' => 'success',
                        'mensaje' => 'insertado',
                        'id' => $id_ultimo->id
                    ];
                    return $data;
                    exit;
                }
            }
            
        }else{
            $data =['response' => 'fail','mensaje' => 'no es post'];
            return $data;
        }
    }

    public function editar(Request $request){
        $data =['response' => 'fail','mensaje' => ''];
        $termino = DB::table('terminos')->where('id', $request->id_termino)->get();
        
        if (count($termino)) {
           $data =['response' => 'success','mensaje' => 'Termino',"termino" => $termino];
        }else{
            $data =['response' => 'fail','mensaje' => 'Termino',"termino" => $termino];
        }
        return  $data;
    }
}
