
// insertar cotización
function insertCotizacion(){
  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

var nombre_c_S = document.getElementById('nombre_c').value;
var razon_soc_S = document.getElementById('razon_soc').value;
var rfc_S = document.getElementById('rfc').value;
var fecha_ven_S = document.getElementById('fecha_ven').value;
var txt_id_plantilla_S = document.getElementById('txt_id_plantilla').value;
var id_cotizacion_S = document.getElementById('id_cotizacion').value;
if (txt_id_plantilla_S == 0) {
   alert("Elije una plantilla");
   exit();
}
  $.ajax({
    type:'POST',
    url:'/insert',
    data:{
      nombre_c: nombre_c_S,
      razon_soc: razon_soc_S,
      rfc: rfc_S,
      fecha_ven: fecha_ven_S,
      txt_id_plantilla: txt_id_plantilla_S,
      id_cotizacion: id_cotizacion_S
    },
    success:function(data){
      console.log(data.mensaje);
      if (data.response === 'success') {
         siguiente('concepto');
         document.getElementById("id_cotizacion").value=data.id;
      }
      indexConcepto();
    }
  });
 }


// indexConcepto();
 // conceptos
 function indexConcepto(){
   var id_cotizacion_S = document.getElementById('id_cotizacion').value;
   $.ajax({
    type:'GET',
    url:'/indexConcepto',
    data:{id_cotizacion: id_cotizacion_S},
    success:function(data){
      console.log(data);
      console.log(data.conceptos.length);
      if (data.response === 'fail') {
        document.getElementById('btn_concepto').disabled=true;
      }else{
        document.getElementById('btn_concepto').disabled=false;
        var row ='';
        var subtotal = 0 ;
        for (var i = 0; i < data.conceptos.length; i++) {
          subtotal = parseInt(subtotal) + parseInt(data.conceptos[i].subtotal);
         row += '<tr>'+
                '<td width="40%">'+data.conceptos[i].concepto+'</td>'+
                '<td width="10%">'+data.conceptos[i].cantidad+'</td>'+
                '<td width="15%">'+data.conceptos[i].p_unitario+'</td>'+
                '<td width="15%">'+data.conceptos[i].subtotal+'</td>'+
                '<td width="30%" class="text-center">'+
                  '<button class="btn btn-success btn-sm" onclick="editar_concepto('+data.conceptos[i].id+');">'+'editar'+'</button>'
                  '<button class="btn btn-success btn-sm" >'+'eliminar'+'</button>'+
                '</td>'+
           '</tr>';
        }
        var iva = (parseInt(subtotal) * 16 ) / 100;
        var total = iva + subtotal;
        document.getElementById('rows').innerHTML = row;
        document.getElementById('subtotal3').innerHTML = subtotal;
        document.getElementById('iva').innerHTML = iva;
        document.getElementById('total').innerHTML = total;
      }
    }
  });
 }


function insertConcepto(){

  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   var concepto_S = document.getElementById('concepto').value;
   var cantidad_S = document.getElementById('cantidad').value;
   var p_unitario_S = document.getElementById('p_unitario').value;
   var subtotal_S = document.getElementById('subtotal').value;
   var id_concepto_S = document.getElementById('id_concepto').value;
   var id_cotizacion_S =  document.getElementById('id_cotizacion').value;

   if (id_cotizacion_S==0) {
      location.href ="http://localhost:8000/home";
      exit();
   }
   if (concepto_S == "" || cantidad_S=="" || p_unitario_S=="" || subtotal_S=="") {
      alert("ingresa los datos");
      exit();
   }

  $.ajax({
    type:'POST',
    url:'/insertConcepto',
    data:{
      concepto: concepto_S,
      cantidad: cantidad_S,
      p_unitario: p_unitario_S,
      subtotal: subtotal_S,
      id_concepto:id_concepto_S,
      id_cotizacion : id_cotizacion_S
    },
    success:function(data){
      console.log(data);
      $('#mdl_concepto').modal('hide');
         document.getElementById('concepto').value="";
         document.getElementById('cantidad').value="";
         document.getElementById('p_unitario').value="";
         document.getElementById('subtotal').value="";
         document.getElementById('subtotal2').innerHTML="0.00";
         // document.getElementById('id_concepto').value="";
         // document.getElementById('id_cotizacion').value="";
      indexConcepto();
      // if (data.mensaje === 'insertado') {
      //   document.getElementById("id_cotizacion").value=data.id;
      // }
    }
  });
 }

function cal_subtotal(){
  var cantidad = 0;
  var p_unitario = 0;
  var subtotal = document.getElementById('subtotal');

  cantidad = document.getElementById('cantidad').value;
  p_unitario = document.getElementById('p_unitario').value;
  var resultado = parseInt(cantidad) * parseInt(p_unitario);
  resultado = parseInt(resultado);
  if (isNaN(resultado)) {
    resultado = 0;
  }

  subtotal.value = resultado;
  document.getElementById('subtotal2').innerHTML = resultado;
}
function cal_subtotal2(){
  var cantidad = 0;
  var p_unitario = 0;
  var subtotal = document.getElementById('editSubtotal');

  cantidad = document.getElementById('editCantidad').value;
  p_unitario = document.getElementById('editP_unitario').value;
  var resultado = parseInt(cantidad) * parseInt(p_unitario);
  resultado = parseInt(resultado);
  if (isNaN(resultado)) {
    resultado = 0;
  }

  subtotal.value = resultado;
  document.getElementById('editSubtotal2').innerHTML = resultado;
}

function editar_concepto(id){
   $.ajax({
    type:'GET',
    url:'/editarConcepto',
    data:{id_concepto: id},
    success:function(data){
      console.log(data.concepto);
      document.getElementById('editConcepto').value= data.concepto[0].concepto;
      document.getElementById('editCantidad').value= data.concepto[0].cantidad;
      document.getElementById('editP_unitario').value= data.concepto[0].p_unitario;
      document.getElementById('editSubtotal2').innerHTML= data.concepto[0].subtotal;
      document.getElementById('editSubtotal').value= data.concepto[0].subtotal;
      document.getElementById('editId_concepto').value= data.concepto[0].id;
      $('#mdl_concepto_editar').modal('show');
    }
  });
}

function actualizarConcepto(){

  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   var concepto_S = document.getElementById('editConcepto').value;
   var cantidad_S = document.getElementById('editCantidad').value;
   var p_unitario_S = document.getElementById('editP_unitario').value;
   var subtotal_S = document.getElementById('editSubtotal').value;
   var id_concepto_S = document.getElementById('editId_concepto').value;

   if (concepto_S == "" || cantidad_S=="" || p_unitario_S=="" || subtotal_S=="") {
      alert("ingresa los datos");
      exit();
   }

  $.ajax({
    type:'POST',
    url:'/insertConcepto',
    data:{
      concepto: concepto_S,
      cantidad: cantidad_S,
      p_unitario: p_unitario_S,
      subtotal: subtotal_S,
      id_concepto:id_concepto_S,
    },
    success:function(data){
      console.log(data);
      $('#mdl_concepto_editar').modal('hide');
         document.getElementById('editConcepto').value="";
         document.getElementById('editCantidad').value="";
         document.getElementById('editP_unitario').value="";
         document.getElementById('editSubtotal').value="";
         document.getElementById('editSubtotal2').innerHTML="0.00";
         indexConcepto();
    }
  });
 }
// terminos
// indexTermino();
 function indexTermino(){
   var id_cotizacion_S =  document.getElementById('id_cotizacion').value;

   $.ajax({
    type:'GET',
    url:'/indexTermino',
    data:{
      id_cotizacion: id_cotizacion_S
    },
    success:function(data){
      siguiente('termino');
       console.log(data);
      var row ='';
      for (var i = 0; i < data.length; i++) {
       row += '<tr style="border-bottom: 1px solid #ECECEC">'+
             '<td width="10%">1</td>'+
              '<td  width="40%">'+data[i].termino+'</td>'+
              '<td width="30%" class="text-center">'+
                '<button class="btn btn-success btn-sm" onclick="editarTermino('+data[i].id+');">editar</button>'+
                '<button class="btn btn-success btn-sm">Eliminar</button>'+
              '</td>'+
            '</tr>';
      }      
      document.getElementById('rowsTermino').innerHTML = row;
    }
  });
 }

function insertTermino(){

  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   var termino_S = document.getElementById('termino').value;
   var id_cotizacion_S =  document.getElementById('id_cotizacion').value;
   var id_termino_S = document.getElementById('id_termino').value;

   if (id_cotizacion_S == 0) {
      location.href ="http://localhost:8000/home";
      exit();
   }
   if (termino_S == "") {
        alert("ingresa tus datos");
        exit();
  }
  $.ajax({
    type:'POST',
    url:'/insertTermino',
    data:{
      termino: termino_S,
      id_cotizacion : id_cotizacion_S,
      id_termino : id_termino_S
    },
    success:function(data){
      console.log(data);
      $('#mdl_termino').modal('hide')
      indexTermino();
      // if (data.mensaje === 'insertado') {
      //   document.getElementById("id_cotizacion").value=data.id;
      // }
    }
  });
 }

function editarTermino(id){
   $.ajax({
    type:'GET',
    url:'/editarTermino',
    data:{id_termino: id},
    success:function(data){
      console.log(data.termino);
      document.getElementById('editTermino').value= data.termino[0].termino;
      document.getElementById('editId_termino').value= data.termino[0].id;
      $('#mdl_termino_editar').modal('show');
    }
  });
}

function actualizarTermino(){
   $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   var id_termino = document.getElementById('editId_termino').value;
   var termino = document.getElementById('editTermino').value;

   if (termino == "") {
      alert("ingresa los datos");
      exit();
   }

  $.ajax({
    type:'POST',
    url:'/insertTermino',
    data:{
      id: id_termino,
      termino: termino
    },
    success:function(data){
      console.log(data);
      $('#mdl_termino_editar').modal('hide');
         document.getElementById('editId_termino').value="";
         document.getElementById('editTermino').value="";
         indexTermino();
    }
  });
}

// Elegir plantilla
// $(".btn_plantilla").click(function(e){
function elegirPlantilla(id_plantilla){
  document.getElementById('txt_id_plantilla').value = id_plantilla;
  var box = document.getElementsByClassName('div_plantilla');
  for (var i=0; i<box.length; i++) box[i].style.backgroundColor="white";
  for (var i=0; i<box.length; i++) box[i].style.color="black";
  document.getElementById(id_plantilla).style.backgroundColor ='#34495E';
  document.getElementById(id_plantilla).style.color ='white';
 }
// });

siguiente('cotizacion');
function siguiente(form){
  var concepto = document.getElementById('form_concepto');
  var cotizacion = document.getElementById('form_cotizacion');
  var termino = document.getElementById('form_termino');

  if (form == 'cotizacion') {
    concepto.style.display = 'none';
    termino.style.display = 'none';
    cotizacion.style.display = 'block';
  }
  if (form == 'concepto') {
    concepto.style.display = 'block';
    termino.style.display = 'none';
    cotizacion.style.display = 'none';
  }
  if (form == 'termino') {
    concepto.style.display = 'none';
    termino.style.display = 'block';
    cotizacion.style.display = 'none';
  }
  
  
  
}