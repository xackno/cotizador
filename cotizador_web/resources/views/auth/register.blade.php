@extends('layouts.app')

@section('content')
<div class="container" style=" max-width:100%;">


    <div id="div_alert2"></div>
    <div class="card" style="width: 100%">
        <div class="card-header bg-info text-white">
         <h3>Adquirir suscripción</h3>
        </div>
        
        <div class="card-body">
            <form method="POST" action="">
                @csrf
                <div class="row">
                    <div class="col">
                        <h3 class="text-dark"><b>suscripciones</b></h3>
                        <div class="row">
                            <div class="col">
                                <div class="card" id="card1">
                                    <div class="card-header bg-primary text-white"><b>BÁSICO</b></div>
                                <div class="card-body" style="font-size: 70%">
                                    <h5 class="text-danger"><b>$100MXN</b></h5>
                                    <p>20 documentos al mes <br>Cotizaciones o facturas</p>
                                    
                                    <p>Personalización de logotipo</p><hr>
                                    <br><br><br>
                                    <a class="btn btn-info btn-sm text-white" id="susc1">Seleccionar</a>
                                </div>
                            </div>
                            </div><!-- fin col  -->
                            <div class="col">
                                <div class="card" id="card2">
                                    <div class="card-header bg-danger text-white"><b>PLUS</b></div>
                                <div class="card-body" style="font-size:70%">
                                    <h5 class="text-danger"><b>$150MXN</b></h5>
                                    <p>50 documentos al mes <br>Cotizaciones o facturas</p>
                                    
                                    <p>Personalización de colores y logotipo</p><hr>
                                    <br><br>
                                    <a class="btn btn-warning btn-sm text-white" id="susc2">Seleccionar</a>
                                </div>
                            </div>
                            </div><!-- fin col -->
                            <div class="col">
                                <div class="card" id="card3">
                                    <div class="card-header bg-info text-white"><b>PREMIUM</b></div>
                                <div class="card-body" style="font-size: 70%">
                                    <h5 class="text-danger"><b>$200MXN</b></h5>
                                    <p>100 documentos al mes <br>Cotizaciones o facturas</p>
                                    
                                    <p>Personalización de colores y logotipo</p><hr>
                                    <p>Soporte de chat</p>
                                    <a class="btn btn-success btn-sm text-white" id="susc3">Seleccionar</a>
                                </div>
                            </div>
                            </div>
                            <!-- fin col -->

                            
                            
                           

                              
                        </div>
                        
                        
                    </div><!-- fin col -->
                    <div class="col">
                        <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre completo">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                    <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>

                    <div class="col-md-6">
                        <input id="user" type="text" class="form-control @error('name') is-invalid @enderror" name="user" required autocomplete="name" autofocus placeholder="Usuario para loguear">
                    </div>
                </div>
               
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo electrónico">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña') }} <i class="" id="icon_check"></i></label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <!-- suscription -->
                <input type="text" name="suscription" id="suscription" class="form-control d-none" >

                <input type="" name="fecha_expiracion" id="fecha_expiracion" class="d-none">


                    </div> <!-- fin col -->
                </div><!-- fin row -->

                
                
                


                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-success float-right">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
            </form>
            <a href="{{ route('login') }}" class="float-right">Ya tengo una cuenta.</a>
        </div>
    </div>

 
</div>
@endsection

@section('script')

<script type="text/javascript">
//funcion obteniendo fecha para agregarlo al plan sumandole un año ############
    function fecha(){
        var fecha = new Date();

        var dia;var mes;var año;
        if (fecha.getDate()<10) {
            dia="0"+fecha.getDate();
        }else{
            dia=fecha.getDate();
        }

        if ((fecha.getMonth()+1)<10) {
            mes="0"+(fecha.getMonth()+1);
        }else{
            mes=fecha.getMonth()+1;
        }
        año=fecha.getFullYear()+1;
        fechahoy=(año+"-"+mes+"-"+dia);
        return fechahoy;
    }

    $("#fecha_expiracion").val(fecha());



    

//####################mostrar un icon check al comprobar contraseña################
    var pss;
    $("#password-confirm").focus(function(){
        pss=$("#password").val(); 
    });
    $("#password-confirm").keyup(function(){
        if ($(this).val()==pss) {
            $("#icon_check").removeClass("fa fa-close text-danger");
            $("#icon_check").addClass("fa fa-check-circle text-success");
        }else{
            $("#icon_check").removeClass("fa fa-check-circle text-success");
            $("#icon_check").addClass("fa fa-close text-danger");
        }
    });

//############opciones para auto seleccionar el plan basico al entrar a la vista de register
    $("#suscription").val("basico");
    $("#card2,#card3").css("background","white");
    $("#card1").css("background","#F9E79F");
//#######################################################
//######################cambiar fondo al seleccionar otro plan#####
    $("#susc1").click(function(e){
        $("#suscription").val("basico");
        $("#card2,#card3").css("background","white");
        $("#card1").css("background","#F9E79F");
    });
    $("#susc2").click(function(e){
         $("#suscription").val("plus");
         $("#card1,#card3").css("background","white");
         $("#card2").css("background","#F9E79F");
        
    });
    $("#susc3").click(function(e){
         $("#suscription").val("premium");
         $("#card1,#card2").css("background","white");
        $("#card3").css("background","#F9E79F");
    });
//###################################################################
</script>

@endsection