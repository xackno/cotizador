<?php

namespace App\Exports;

use App\productos;
use Maatwebsite\Excel\Concerns\FromCollection;

class productoExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return productos::all();
    }
}
