<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Concepto;

class ConceptoController extends Controller
{
    public function index(Request $request){
    	$data =['response' => 'fail','mensaje' => ''];
        $conceptos = DB::table('conceptos')->where('cotizacion_id', $request->id_cotizacion)->get();

        if (count($conceptos)) {
           $data =['response' => 'success','mensaje' => 'Concepto',"conceptos" => $conceptos];
        }else{
            $data =['response' => 'fail','mensaje' => 'Concepto',"conceptos" => $conceptos];
        }
      	return  $data;
    }
    public function insertar(Request $request){
        if($request->isMethod('post')){
            $concepto =  new Concepto;
            $concepto->cotizacion_id = $request->id_cotizacion;
            $concepto->concepto =$request->concepto;
            $concepto->cantidad = $request->cantidad;
            $concepto->p_unitario = $request->p_unitario;
            $concepto->subtotal = $request->subtotal;
            $concepto->iva = 16;
            $concepto->total = 0;
            $id_concepto = $request->id_concepto;


            $data =['response' => 'fail','mensaje' => ''];
            $isRegistrer = DB::table('conceptos')->where('id',$id_concepto)->exists();

            if ($isRegistrer) {
                $isActualizado = Concepto::where('id', $id_concepto)
                ->update([
                    'concepto' => $concepto->concepto,
                    'cantidad' => $concepto->cantidad,
                    'p_unitario' => $concepto->p_unitario,
                    'subtotal' => $concepto->subtotal
                ]);
                $data =[
                    'response' => 'success',
                    'mensaje' => 'actualizado',
                    'id_concepto' => $id_concepto
                ];
                return $data;
                exit;
            }else{
                $guardar = $concepto->save();
                $id_ultimo = Concepto::latest('id')->first();
                if (json_decode($guardar)) {
                    $data =[
                        'response' => 'success',
                        'mensaje' => 'insertado',
                        'id' => $id_ultimo->id
                    ];
                    return $data;
                    exit;
                }
            }
            
        }else{
            $data =['response' => 'fail','mensaje' => 'no es post'];
            return $data;
        }
    }
    public function editar(Request $request){
        $data =['response' => 'fail','mensaje' => ''];
        $concepto = DB::table('conceptos')->where('id', $request->id_concepto)->get();
        
        if (count($concepto)) {
           $data =['response' => 'success','mensaje' => 'Concepto',"concepto" => $concepto];
        }else{
            $data =['response' => 'fail','mensaje' => 'Concepto',"concepto" => $concepto];
        }
        return  $data;
    }
}
