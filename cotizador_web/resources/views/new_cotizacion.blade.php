@extends('layouts.app')

@section('content')
<style>
	#tb-iva th,td{
		border-top: none !important;
	}
</style>
<div class="container">
	<div class="col-md-12 mt-5 mb-3">
		<h1 class="text-center"><small>Elije una plantilla</small> </h1>
	</div>
	<!-- Agregar cotizacion -->
	<div class="col-md-12 bg-white py-3 " style="border-top:7px solid #34495E;" id="form_cotizacion" >
		<div class="row d-flex justify-content-center" >
			<div class="col-md-2 m-2 d-flex align-items-center" style="border: 1px solid #DDDDDD; border-style: dashed;border-width: 3px;">
				<div>
					<h3 class="text-center">Agregar  plantilla</h3>
				</div>
			</div>
			<div class="col-md-2 p-2 m-2 div_plantilla" style="border: 1px solid #DDDDDD;" id="1">
				<div>
					<img src="{{asset('img/plantilla2.jpg')}}" alt="" width="100%">
					<p>Nombre de la plantilla</p>
				</div>
				<div><button class="btn btn-sm btn-outline-info btn-block" onclick="elegirPlantilla(1)">Elegir</button></div>
			</div>
			<div class="col-md-2 p-2 m-2 div_plantilla" style="border: 1px solid #DDDDDD;" id="2">
				<div>
					<img src="{{asset('img/plantilla2.jpg')}}" alt="" width="100%">
					<p>Nombre de la plantilla</p>
				</div>
				<div><button class="btn btn-sm btn-outline-info btn-block" onclick="elegirPlantilla(2)">Elegir</button></div>
			</div>
			<div class="col-md-2 p-2 m-2 div_plantilla" style="border: 1px solid #DDDDDD;" id="3">
				<div>
					<img src="{{asset('img/plantilla2.jpg')}}" alt="" width="100%">
					<p>Nombre de la plantilla</p>
				</div>
				<div><button class="btn btn-sm btn-outline-info btn-block" onclick="elegirPlantilla(3)">Elegir</button></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 offset-md-2 p-3 mt-5">
				<form class="" method="POST" action="javascript:void(0)">
					
					<div class="form-group">
						<label for="">Nombre de cotización</label>
						<input type="text" class="form-control" name="nombre_c" id="nombre_c" required="" value="{{$cotizacion[0]->nombre ?? ''}}">
					</div>
					<div class="form-group">
						<label for="">Nombre del cliente ó Razón social</label>
						<input type="text" class="form-control" name="razon_soc" id="razon_soc" required="" value="{{ $cotizacion[0]->razon_soc ?? ''}}">
					</div>
					<div class="form-group">
						<label for="">RFC del cliente</label>
						<input type="text" class="form-control" name="rfc" id="rfc" value="{{$cotizacion[0]->rfc ?? ''}}">
					</div>
					<div class="form-group">
						<label for="">Fecha de vencimiento</label>
						<input type="date" class="form-control" name="fecha_ven" id="fecha_ven" required="" value="{{$cotizacion[0]->fecha_ven ?? ''}}">
					</div>
					<div class="text-right d-flex justify-content-between">
						<input type="text" name="id_plantilla"  id="txt_id_plantilla"  value="{{$cotizacion[0]->plantilla_id ?? ''}}" hidden="">
						<input type="text" name="id_cotizacion"  id="id_cotizacion"  value="{{$cotizacion[0]->id ?? 0}}" hidden="">
						<a href="/home" class="btn btn-warning" id="" >Salir</a>
						<button class="btn btn-success" id="" onclick="insertCotizacion();">siguiente</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- agregar concepto -->
	<div class="col-md-12 bg-white py-3 mt-4 " style="border-top:7px solid #34495E" id="form_concepto">
		<div class="row">
			<div class="col-md-12  p-3">
				<table class="table table-hover" width="300px" >
				  <thead>
				    <tr>
				      <th width="40%">Concepto</th>
				      <th width="10%">Cantidad</th>
				      <th width="15%">P. Unitario</th>
				      <th width="15%">Subtotal</th>
				      <th width="30%" class="text-center">Acciones</th>
				    </tr>
				  </thead>
				  <tbody id="rows">
				  </tbody>
				  <tr style="background-color: transparent; cursor: pointer;">
				      <td colspan="5">
				      	<p class="text-center py-2" style="border: 1px solid #ECECEC;border-style: dashed;border-width: 2px;" data-toggle="modal" data-target="#mdl_concepto">Agregar concepto</p>
				      </td>
				    </tr>
				</table>
			</div>
			<div class="col-md-12 p-3 mt-3">
				<table class="table" width="300%" id="tb-iva" >
				  <thead>
				  <tbody>
				  	<tr>
				      <td width="40%"></td>
				      <td width="10%"></td>
				      <td width="15%"></td>
				      <td width="15%" style="background-color: #DFE9EB"><b>Subtotal</b></td>
				      <td width="30%" class="text-center" style="background-color: #DFE9EB"><p>$<span id="subtotal3"> 0.00</span></p></td>
				    </tr>
				    <tr>
				      <td width="40%"></td>
				      <td width="10%"></td>
				      <td width="15%"></td>
				      <td width="15%" style="background-color: #DFE9EB"><b>IVA 16 %</b></td>
				      <td width="30%" class="text-center" style="background-color: #DFE9EB"><p>$<span id="iva"> 0</span></p></td>
				    </tr>
				    <tr>
				      <td width="40%"></td>
				      <td width="10%"></td>
				      <td width="15%"></td>
				      <td width="15%" style="background-color: #DFE9EB"><b>Total</b></td>
				      <td width="30%" class="text-center" style="background-color: #DFE9EB"><p>$ <span id="total"> 0</span></p></td>
				    </tr>
				  </tbody>
				</table>
				<div class=" text-right d-flex justify-content-between">
					<button class="btn btn-warning" onclick="siguiente('cotizacion');">Anterior</button>
					<button class="btn btn-success" onclick="indexTermino();" id="btn_concepto">Siguiente</button>
				</div>
			</div>
		</div>
		<br>
	</div>
	<!-- terminos y condiciones -->
	<div class="col-md-12 bg-white py-3 mt-4 " style="border-top:7px solid #34495E" id="form_termino">
		<div class="row">
			<div class="col-md-12 p-3">
				<table class="table table-hover" width="300px" >
				  <thead style="border: none;">
				    <tr>
				      <th style="border: none" width="10%"></th>
				      <th style="border: none" width="40%"></th>
				      <th style="border: none" width="30%" class="text-center"></th>
				    </tr>
				  </thead>
				  <tbody id="rowsTermino">
				  	<!-- <tr style="border-bottom: 1px solid #ECECEC">
				  	  <td width="40%"></td>
				      <td  width="40%">Sistema de socios</td>
				      <td width="30%" class="text-center">
				      	<button class="btn btn-success btn-sm">editar</button>
				      	<button class="btn btn-success btn-sm">editar</button>
				      </td>
				    </tr>
				    <tr style="border-bottom: 1px solid #ECECEC">
				    	<td></td>
				      <td width="40%">Sistema de socios</td>
				      <td width="30%" class="text-center">
				      	<button class="btn btn-success btn-sm">editar</button>
				      	<button class="btn btn-success btn-sm">editar</button>
				      </td>
				    </tr> -->
				  </tbody>
				</table>
			</div>
			<div class="col-md-6 offset-md-3  p-3" style="cursor: pointer">
				<p class="text-center py-4" style="border: 1px solid #ECECEC;border-style: dashed;border-width: 2px;" data-toggle="modal" data-target="#mdl_termino">Agregar Término o condición</p>
			</div>
			<div class="col-md-12 text-right d-flex justify-content-between">
				<button class=" btn btn-warning" onclick="siguiente('concepto')">Anterior</button>
				<a href="/home" class="btn btn-success">Vista previa</a>
			</div>
		</div>
		<br>
	</div>
</div>
<style>
	.modal-lg{
		max-width: 70% !important; 
	}
</style>
<!-- Modal concepto Agregar-->
<div class="modal fade" id="mdl_concepto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      
      <!-- <div class="row" style="border: 1px solid red; background-color: none;">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
	</div> -->

      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-4">
       			<p>Concepto</p>
       			<!-- <input type="text" class="form-control" id=""> -->
       			<textarea name="concepto" class="form-control" id="concepto" cols="20" rows="2"></textarea>
       		</div>
       		<div class="col-md-2">
       			<p>Cantidad</p>
       			<input type="text" name="cantidad" class="form-control" id="cantidad" oninput="cal_subtotal()">
       		</div>
       		<div class="col-md-2">
       			<p>P. Unitario</p>
       			<input type="text" name="p_unitario" class="form-control" id="p_unitario" oninput="cal_subtotal()">
       		</div>
       		<div class="col-md-2 text-center">
       			<p>Subtotal</p>
       			<p> $ <b id="subtotal2">0.00</b></p>
       			<input type="text" name="subtotal" id="subtotal" hidden="">
       		</div>
       		<input type="text" name="id_concepto" id="id_concepto" value="0">
       		<div class="col-md-2 d-flex align-items-center">
       			<button class="btn btn-sm btn-success mx-1" onclick="insertConcepto()">OK</button>
       			<button class="btn btn-sm btn-danger mx-1" data-dismiss="modal">No</button>
       		</div>
       	</div>
      </div>
    </div>
  </div>
</div>

<!-- Modal concepto Editar-->
<div class="modal fade" id="mdl_concepto_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      
      <!-- <div class="row" style="border: 1px solid red; background-color: none;">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
	</div> -->

      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-4">
       			<p>Concepto</p>
       			<!-- <input type="text" class="form-control" id=""> -->
       			<textarea name="concepto" class="form-control" id="editConcepto" cols="20" rows="2"></textarea>
       		</div>
       		<div class="col-md-2">
       			<p>Cantidad</p>
       			<input type="text" name="cantidad" class="form-control" id="editCantidad" oninput="cal_subtotal2()">
       		</div>
       		<div class="col-md-2">
       			<p>P. Unitario</p>
       			<input type="text" name="p_unitario" class="form-control" id="editP_unitario" oninput="cal_subtotal2()">
       		</div>
       		<div class="col-md-2 text-center">
       			<p>Subtotal</p>
       			<p> $ <b id="editSubtotal2">0.00</b></p>
       			<input type="text" name="subtotal" id="editSubtotal" >
       		</div>
       		<input type="text" name="id_concepto" id="editId_concepto" value="0">
       		<div class="col-md-2 d-flex align-items-center">
       			<button class="btn btn-sm btn-success mx-1" onclick="actualizarConcepto()">OK</button>
       			<button class="btn btn-sm btn-danger mx-1" data-dismiss="modal">No</button>
       		</div>
       	</div>
      </div>
    </div>
  </div>
</div>


<!-- Modal terminos -->
<div class="modal fade" id="mdl_termino" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-6">
       			<p>Término</p>
       			<!-- <input type="text" class="form-control" id=""> -->
       			<textarea name="termino" class="form-control" id="termino" cols="20" rows="2"></textarea>
       		</div>
       		<input type="text" name="id_termino" id="id_termino" value="0">
       		<div class="col-md-2 d-flex align-items-center">
       			<button class="btn btn-sm btn-success mx-1" onclick="insertTermino()">OK</button>
       			<button class="btn btn-sm btn-danger mx-1" data-dismiss="modal">No</button>
       		</div>
       	</div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Editar terminos -->
<div class="modal fade" id="mdl_termino_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-6">
       			<p>Término</p>
       			<!-- <input type="text" class="form-control" id=""> -->
       			<textarea name="termino" class="form-control" id="editTermino" cols="20" rows="2"></textarea>
       		</div>
       		<input type="text" name="id_termino" id="editId_termino" value="0">
       		<div class="col-md-2 d-flex align-items-center">
       			<button class="btn btn-sm btn-success mx-1" onclick="actualizarTermino()">OK</button>
       			<button class="btn btn-sm btn-danger mx-1" data-dismiss="modal">No</button>
       		</div>
       	</div>
      </div>
    </div>
  </div>
</div>
@endsection
