@extends('layouts.app')

@section('content')
<style>
	.box-info1{
		border: 1px solid grey;
		border-right: none;
		border-radius: 7px 0px 0px 7px;
	}
	.box-info2{
		border: 1px solid grey;
		border-radius: 0px 7px 7px 0px;
	}
</style>
<div class="container-fluid">
	<div class="row  mt-5">
		<div class="col-md-8 col-sm-12 col-xs-12 d-flex justify-content-between py-4 bg-white box-info1">
			<div class="col-md-3" style="border: 1px solid grey">
				<img src="{{asset('img/empresa.png')}}" alt="" width="100%">
			</div>
			<div class="col-md-6">
				<h3>Nombre de mi empresa</h3>
				<p>Información fiscal</p>
				<p>Nombre usuario</p>
				<p>Email</p>
			</div>
			<div class="col-md-3">
				<button class="btn btn-sm btn-info">Editar mi cuenta</button>
				<p>cerrar sesión</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 bg-white py-3 box-info2">
			<div class="col-12 d-flex justify-content-between text-center" style="border-bottom: 1px solid grey">
				<div>
					<p>Plan: Premium <br><small>Expira el: 19/08/2021</small></p>
				</div>
				<div>
					<h4>Elige tu plan</h4>
				</div>
			</div>
			<div class="col-12 text-center py-3">
				<div class="col-md-12">
					<h1>3567</h1>
				</div>
				<div class="col-md-12">
					<p><small>DOCUMENTOS RESTANTES</small></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 mt-5">
			<h3><b>Mis cotizaciones</b></h3>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12 bg-white py-3 text-right">
			<a href="/nuevo" class="btn btn-success">Nueva cotización</a>
			<a href="/PDF" target="_blank" class="btn btn-info">PDF</a>
			<a href="/PDF_guardar" target="_blank" class="btn btn-info">Guardar</a>
			<a href="/PDF_descargar" target="_blank" class="btn btn-info">Descargar</a>
			<a href="/PDF_preview" target="_blank" class="btn btn-info">HTML</a>
		</div>
		<div class="col-md-12 bg-white py-3">
			<table class="table table-striped table-bordered ">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Proyecto</th>
			      <th scope="col">Fecha</th>
			      <th scope="col">Status</th>
			      <th scope="col">Total</th>
			      <th scope="col">Acciones</th>
			    </tr>
			  </thead>
			  <tbody>
			  @foreach( $cotizaciones as $i => $C)
			    <tr>
			      <th scope="row">{{++$i}}</th>
			      <td>{{$C->nombre}}</td>
			      <td>{{$C->fecha_ven}}</td>
			      <td>{{$C->status}}</td>
			      <td>{{$C->total}}</td>
			      <td><a href="{{ url('/editar/'.$C->id) }}" class="btn btn-sm btn-warning" > Editar</a>
			      <button class="btn btn-sm btn-danger"> Eliminar</button></td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
	</div>
	<br><br>
</div>
@endsection
