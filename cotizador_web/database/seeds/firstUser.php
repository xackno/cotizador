<?php

use Illuminate\Database\Seeder;
use App\locales;
use App\User;



class firstUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->delete();
        DB::statement("ALTER TABLE users AUTO_INCREMENT = 1;");
        User::create([
            'name' => 'admin',
            'user' => 'admin',
            'email' => 'xackno1995@gmail.com',
            'password' => Hash::make('admin123'),
            'type' => 'superadmin',
            'local'=>1,
            'foto'=>'boy.png',
            'system'=>1,
        ]);

    }
}
