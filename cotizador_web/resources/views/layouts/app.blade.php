<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta lang="es">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Cotizador') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    


    <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.css')}}">


</head>
<body >
    <div id="app">

        @if( Request::path()!='login')

        <nav class="navbar navbar-expand-md navbar-light shadow-sm" style="background-color: #34495E; color:white !important">
            <div>
                <img src="{{asset('public/img/logo.jpg')}}" style="width: 30px">
            </div>




            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}" style="color: white">
                    @if(Request::path() !='register')
                    {{$menu}}
                    @endif
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        @guest
                            
                            @if (Route::has('register'))
                                <li class="nav-item" style="padding-right:5px ">
                                    <a class="nav-link btn btn-outline-danger text-white" href="{{ route('register') }}">{{ __('Registrate') }}</a>
                                </li>
                            @endif
                            <li class="nav-item" style="padding-right:5px">
                                <a class="nav-link btn btn-outline-info text-white" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: white">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @endif

        




        <main class="mx-5">
            @yield('content')
        </main>
    </div>
</body>


<script type="text/javascript" src="public/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="public/js/bootstrap.min.js"></script>
@yield('script')
<script type="text/javascript" src="public/script.js"></script>

</html>
